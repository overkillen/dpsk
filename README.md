# Symulacja modulacji różnicowej PSK #

Demonstrator modulacji różnicowej DPSK oraz porównanie go z modulacją BPSK. Testy są przeprowadzane w kanale AWGN.

### Jak uruchomić? ###

Aby uruchomić program, należy uruchomić program Matlab. Następnie należy przejść do folderu, w którym znajduje się plik *modemDPSK*. Ostatnim krokiem jest wpisanie w *Command Window* programu Matlab, polecenia *run modemDPSK*.


### Autorzy ###

* Michał Płatek
* Jakub Sroka
function [ bity] = demodBPSK( zakodowane2 )

    T = 0.2;                     %okres
    time =0.001:0.001:T;    %czas
    E = 5;                      %energia sygna�u
    m = 7;                      %liczba ca�kowita do cz�stotliwo�ci
    fc = m/T;                   %cz�stotliwo��
    l = length(time);           %dlugosc wektora czasu
    nosny = cos(2*pi*fc*time);  %sygna� no�ny potrzebny do operacji mno�enia
                                %z sygna�em odebranym 
    %plot(nosny)                           
    
    ile = length(zakodowane2(2,:))/length(time); %obliczenie ile bit�w zosta�o
                                %zakodowanych w danym sygnale wej�ciowym
    sample = zakodowane2(2,1:l); %tutaj pobieram pr�bk� sygna�u zakodowanego 
                                %o d�ugo�ci trwania  jednego bitu                            
    wymnozony=sample.*nosny;    %mno�enie sygna�u no�nego przez odebrany sygna�
    %plot(wymnozony)             % testowe sprawdzenie czy wszystko dzia�a do tej pory 
    
    pole = trapz(time,wymnozony);  % obliczenie pola pod wykresem funkji
    temp=[];                   %deklaracja wektora w kt�rym b�dzie przechowywany 
                                %odzyskany sygna� cyfrowy o wato�ci "0" lub "1" 
                                
    if(pole>0)
            temp = [temp 1];    % je�li pole pod wykresem funkcji dodatnie to by�a 1 nadana
       else
            temp = [temp 0];    % je�li pole pod wykresem funkcji dodatnie to by�o 0 nadana 
        end                           
                                
   for i=1:ile-1               %dekodowanie sygna�u odebranego dla kolejnych okres�w 
        
    sample = zakodowane2(2,l*(i):l*(i+1)-1); 
   
        wymnozony=sample.*nosny;
        pole = trapz(time,wymnozony);
        if(pole>0)
            temp = [temp 1];
        else
            temp = [temp 0];
        end
   end    
    
    %temp=temp(2:end);       %wyci�cie z wektora wyjsciowego pierwszego elementu
                             %dzi�ki deklaracji wektora w jako temp[],
                             %ju� niepotrzebne 
    bity=temp;              %zrzutowanie pomocniczego wektora na wektor wyj�ciowy                         
                                
                                
end
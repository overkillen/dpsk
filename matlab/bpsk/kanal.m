function [ zakodowane2 ] = kanal(zakodowane,snrdB,fb,fs )
% zakodowane - sygna� 
% fb -  bit/sekund�
% fs -  cz�stotliwo�� pr�bkowania(sampling rate)
% snrdB - SNR w dB

ebn0=10^(snrdB/10);  %zamiana warto�ci t�umienia z logarytm�w  
                     %na warto�ci standardowe liniowe
eb = sum(zakodowane(2,:).^2)/(length(zakodowane(2,:))* fb); %wyliczenie
                     % energii przypadaj�cej na jeden bit
n0=eb/ebn0;          %wyliczenie widmowej g�sto�ci mocy szumu 
pn=n0*fs/2;          %wyliczenie �redniej mocy szum�w, gdzie przepustowo�� 
                     %szum�w jest r�wna po�owie cz�stotliwo�ci pr�bkowania
zakodowane2=[zakodowane(1,:);sqrt(pn)*randn(1,length(zakodowane(1,:)))];
                      %wyliczenie wektora szum�w o d�ugo�ci wektora 
                      %z zakodowan� informacj�                     
zakodowane2(2,:)=zakodowane2(2,:)+zakodowane(2,:); % dodanie wektora szumu do wektora zakodowanej informacji 
end


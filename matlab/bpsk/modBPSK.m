function [ zakodowane ] = modBPSk( bity )


T=0.2;       %okres
E=5;        %energia sygna�u
m=7;        %liczba ca�kowita wykorzystywana przy cz�stotliwo�ci 
f=m/T;      %cz�stotliwo�� 

przedzial=0.001:0.001:T; %przedzia� czasu dla no�nej(odpowiednio dok�adny)

nosny= sqrt(2*(E/T))*cos(2*pi*f*przedzial); %sygna� no�ny

NRZ=bity.*2-1; %zamiana kodu wej�ciowego (typu 1 0 1 ...) na NRZ (typu (1 -1 1 ..)

zakod=[];                      % zadeklarowanie wektora zakod (wa�ne!!)
for i=1:length(bity)            % sprawdzam d�ugo�� sygna�u wej�ciowego
    if(NRZ(i)==-1)              % je�li -1 to odwracam faz� sygna�u
        zakod=[zakod -nosny];   %
    else                        %
        zakod=[zakod nosny];    %je�li 1 to nie odwracam fazy sygna�u
    end
end

%zakod= zakod(2:end);            %usuni�cie pierwszego elementu wektora 
                                %zadeklarowanej przed p�tl� for
                                %dzi�ki deklaracji wektora w formie zakod[],
                                %ju� niepotrzebne 
                                
%zakod=awgn(zakod,10);           %dodanie bia�ego szumu do zakodowanej informacji                                
czasymulacji=0.001:0.001:length(bity)*T;  %dodatkowy wektor posiadaj�cy 
                                               %czas symulacji ca�ego
                                               %sygna�u
                                               %wej�ciowego 

%plot(czasymulacji,zakod)        %czytelniejsze wy�wietlenie osi X                       

%zakodowane= zakod;              %wys�anie informacji zakodowanej do zmiennej
                                % wyj�ciowej funkcji 
zakodowane=[czasymulacji;zakod];%wys�anie informacji zakodowanej do zmiennej
                                % wyj�ciowej funkcji, dodatkowo wysy�am ca�y 
                                % czas symulacji
end


function [ ] = odzysknosnej( bity, snr)
zmodulowany = modBPSK(bity);
zakodowane2 = kanal(zmodulowany,snr,5,1000);

f1 = figure(1);
setPosition(f1,true);
subplot(2,1,1)
plot(zakodowane2(1,:),zakodowane2(2,:));
title('Sygnal po modulacji i po przejsciu przez kanal AWGN')

kwadrat2 = zakodowane2(2,:).^2; %podnosz� sygna� do kwadratu (zaczynam 
                                   %procedur� odzyskania no�nej
subplot(2,1,2)
plot(zakodowane2(1,:),kwadrat2)
title('Sygnal podniesiony do kwadratu')

filtrsrod=fir1(512,[69 71]/(1000/2)); %tworzenie filtru, element 1000/2 to 
                                      %normalizacja, sygna� by� do kwadratu
                                      %wi�c no�na 35Hz*2=70Hz, ustalam
                                      %granice od 69Hz do 71Hz
                                    
pofiltrowaniu=filter(filtrsrod,1,kwadrat2); %filtrowanie sygna�u 
x(1,:)=zakodowane2(1,1:length(zakodowane2(1,:))-255); %zmniejszam sygna� czasu
                                     %o odpowiednie op�nienie, wyliczone
                                     %jako stopie� wielomianu przy filtrze
                                     %(512) podzielony przez 2.
x(2,:)=pofiltrowaniu(1,256:length(zakodowane2(1,:))); %przesuwam sygna� zakodowany
                                     %o okre�lon� liczb� pr�bek, ma to na
                                     %celu wyeliminowanie b��du
f2=figure(2);           %pocz�tkowego
setPosition(f2,false);
subplot(2,1,1);
plot(x(1,:),x(2,:))
title('Sygnal po filtrowaniu + delay');

nosna=x(2,:)./2;              %obni�am amplitud� aby jej warto�� zgadza�a si�
                                   %z sygna�em pocz�tkowym
nowanosna=0.5:0.5:length(nosna);            %bardziej 'g�sta' no�na, b�dzie potrzebna 
                                   %przy interpolacji
nosna=interp1(1:length(nosna),nosna,nowanosna); % interpoluj� no�n�, zwi�kszam liczb� pr�bek 
                                   %do 2000 i nast�pnie wycinam 1000,
                                   %dzi�ki temu zmniejszam cz�stotliwo��
                                   %sygna�u o po�ow� 
nosna(1)=nosna(2);                 %problem z pierwszym elementem wektora, zapisuj� 
                                   %do niego warto�� 2 elementu
subplot(2,1,2);
plot(x(1,1:200),nosna(1,1:200))
title('Odzyskana nosna ')

end
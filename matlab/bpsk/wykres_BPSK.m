function [ wynik] = wykres_BPSK( we,snrdB)
addpath(genpath('../'));
zakodowane = modBPSK(we);
zakodowane2 = kanal(zakodowane,snrdB,5,1000);
bity = demodBPSK(zakodowane2);


f1 = figure(1);
setPosition(f1,true);
subplot(2,1,1)
plot(zakodowane(1,:),zakodowane(2,:));
title('Sygnal zmodulowany BPSK')
xlabel('czas(s)')

subplot(2,1,2)
plot(zakodowane2(1,:),zakodowane2(2,:));
title('Sygnal zmodulowany w kanale AWGN')
xlabel('czas(s)')


f2 = figure(2);
setPosition(f2,false);

subplot(2,1,1)
stem(we,'r','LineWidth',5);
title('Bity wejsciowe')

subplot(2,1,2)
stem(bity,'g','LineWidth',5);
title('Bity po demodulacji')
wynik=bity;
end


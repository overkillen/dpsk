
function [n]=awgn_custom(x,snrdB,fb,fs)
    % x - sygna� 
    % fb -  bit/sekund�
    % fs -  sampling rate
    % snrdB - SNR w dB
    ebn0=10^(snrdB/10); %zamiana warto�ci t�umienia z logarytm�w 
    eb = sum(x.^2)/(length(x)* fb);%wyliczenie energii przypadaj�cej na jeden bit
    n0=eb/ebn0; %wyliczenie widmowej g�sto�ci mocy szumu
    sigma=n0*fs/2;  
    pn=n0*fs/2;  %wyliczenie �redniej mocy szum�w
    n=sqrt(pn)*randn(1,length(x));

end
function [wynik ] = ber_DPSK( bitsLength )
%Funkcja por�wnuje teoretyczne prawdopodobie�stwo b��du z symulacj� 
random_bits = randi([ 0 1],1,bitsLength); %generowanie losowych bit�w
c=[]; % wektor przechowuje ilo�� r�zni�cych si� bit�w mi�dzy random_bits a odzyskanymi po demodulacjii
SNRdB = -4:13;
simulatedPB=zeros(10,length(SNRdB)); %wektor prawdopodobie�stw z symulacji

o = modDPSK(random_bits);
for n=1:100
    licznik=1;
    for i=SNRdB(1):SNRdB(length(SNRdB));
        
        oSNR = channel(o,i);
        wyn = demodDPSK(oSNR);
        c = random_bits==wyn; %por�wnanie bit�w wej�ciowych do otrzymanych
        c = c(c==0);%zliczenie r�nic mi�dzy bitami wejsciowymi a otrzymanymi
        p = length(c)/bitsLength; %obliczane prawdopodobie�stwo
        simulatedPB(n,licznik)=p;
        licznik=licznik+1;
        
    end
end
    simulatedPB = mean(simulatedPB);
    
    %oblicznie warto�ci teoretycznych
    EbN0dB=SNRdB(1):SNRdB(length(SNRdB));
    EbN0lin=10.^(EbN0dB/10);
    Pb = 0.5*exp(-EbN0lin);

    figure
    semilogy(SNRdB,simulatedPB,'x',EbN0dB ,Pb);
    title('Bit error rate DPSK')
    legend('Doswiadczalnie','Teoretycznie')
    xlabel('Eo/No [dB]');
    ylabel('BER')
    wynik=simulatedPB ;
end


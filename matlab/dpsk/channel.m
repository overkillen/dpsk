function [ out ] = channel( modulated_signal,snrdB )
%CHANNEL Summary of this function goes here
%   Detailed explanation goes here
n = awgn_custom(modulated_signal(2,:),snrdB,5,1000);
modulated_signal(2,:) = modulated_signal(2,:)+n;
out = modulated_signal;

end


function [ bits] = demodDPSK( signal )

    T = 0.2;                                   %okres
    time = 0.001:0.001:T;            %czas 
    m = 10;                                  %liczba ca�kowita do cz�stotliwo�ci
    fc = m/T;                               %cz�stotliwo��
    ref1 = cos(2*pi*fc*time);  %sygna�y referencyjne
    ref2 = sin(2*pi*fc*time);    %sygna�y referencyjne
    l = length(time);
    b=[1];                                      %ustawienie bitu referencyjnego
    liczba = length(signal(1,:))/length(time);
    for i=0:liczba-2
        if(i==0)
            sample = signal(2,1:l);     %pr�bka sygna�u n-1
            sample2 = signal(2,l:2*l-1); %pr�bka sygna�u n
        else
            sample = signal(2,l*i:l*(i+1)-1);
            sample2 = signal(2,l*(i+1):l*(i+2)-1);
        end
    
        w0=sample.*ref1;
        w1=sample2.*ref1;
        z0=sample.*ref2;
        z1=sample2.*ref2;
    
        w0 = trapz(time,w0);
        w1 = trapz(time,w1);
        z0 = trapz(time,z0);
        z1 = trapz(time,z1);
        temp = ( w0+w1)^2+(z0+z1)^2-( w0-w1)^2-(z0-z1)^2;
        if(temp>0)
            b = [b 1];
        else
            b = [b 0];
        end
    end
    bits = decodeBits(b);
end


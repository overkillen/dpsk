function[encoded] = encodeBits(inputBits)
    %encoded - zakodowane bity(dk)
    %inputBits - bity do zakodowania(ak)
    dk(1) = 1;  %bit referencyjny
        for i=2:length(inputBits)+1
            dk(i) = not(xor(inputBits(i-1),dk(i-1)));
        end
    encoded = dk;
end
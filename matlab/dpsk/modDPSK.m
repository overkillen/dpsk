function [signal] =  modDPSK(bits)
    %modulacja DPSK
    T = 0.2;                        %okres warunek doda� na te �eby nie by� w stylu 0.33(3)
    time = 0.001:0.001:T;   %czas
    E = 1;                   %energia sygna�u
    m = 10;                    %liczba ca�kowita do cz�stotliwo�ci
    fc = m/T;               %cz�stotliwo��
    encode = encodeBits(bits);  %zakodowane bity za pomoc� XNOR
    NRZ = encode.*2-1;              %zamiana na NRZ code
    alltime = 0.001:0.001:length(encode)*T; %ca�y czas
    carry = sqrt(E/(2*T))*cos(2*pi*fc*time); %generowanie no�nej przez okres T
    out = [];
    sign = 1;
    for i=1:length(encode)
        if(NRZ(i)==-1)
            sign = -sign;
        end
        b = sign*carry;
        out = [out b];
          
    end
  
    
   % n = awgn_custom(out,snr,1/T,1000);
    %out = out+n;
    %plot(alltime,out,alltime,o2)
    out = [alltime;out];
    signal=out;
end
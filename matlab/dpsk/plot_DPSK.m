function [ wynik] = plot_DPSK( bits,snrdB)
%Wy�wietla wykresy dla DPSK, czyli sygna� zmodulowany,w awgn, bity wys�ane
%i otrzymane
  addpath(genpath('../'));
  o = modDPSK(bits);
  oSNR = channel(o,snrdB);
  wyn = demodDPSK(oSNR);
  
  
  f1 = figure(1);
 setPosition(f1,true);
  subplot(2,1,1)
  plot(o(1,:),o(2,:));
  title('Sygnal zmodulowany DPSK')
  xlabel('czas(s)')
  subplot(2,1,2)
  plot(oSNR(1,:),oSNR(2,:));
  title('Sygnal zmodulowany w kanale AWGN')
  xlabel('czas(s)')
  
  f2 = figure(2);
  setPosition(f2,false);
  subplot(2,1,1)
  subplot(2,1,1)
  stem(bits,'r','LineWidth',5);
  title('Bity wejsciowe')
  subplot(2,1,2)
  stem(wyn,'g','LineWidth',5);
  title('Bity po demodulacji')
  wynik=wyn;
end


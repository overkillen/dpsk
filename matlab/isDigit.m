function [ out] = isDigit( in)
%Funkcja sprawdza czy string jest liczb�
% 
if isempty(in)
    out =false;
    return;
end
if in(1)=='-'
    if isstrprop(in(2:end),'digit')
        out = true;
    else
        out = false;
    end
else
    if isstrprop(in,'digit')
        out=true;
    else
        out=false;
end

end


function varargout = modemDPSK(varargin)
% MODEMDPSK MATLAB code for modemDPSK.fig
%      MODEMDPSK, by itself, creates a new MODEMDPSK or raises the existing
%      singleton*.
%
%      H = MODEMDPSK returns the handle to a new MODEMDPSK or the handle to
%      the existing singleton*.
%
%      MODEMDPSK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MODEMDPSK.M with the given input arguments.
%
%      MODEMDPSK('Property','Value',...) creates a new MODEMDPSK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before modemDPSK_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to modemDPSK_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools modemDPSK.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help modemDPSK

% Last Modified by GUIDE v2.5 16-Jun-2014 19:22:24

% Begin initialization code - DO NOT EDIT
addpath(genpath('dpsk'));
addpath(genpath('bpsk'));
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @modemDPSK_OpeningFcn, ...
                   'gui_OutputFcn',  @modemDPSK_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before modemDPSK is made visible.
function modemDPSK_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to modemDPSK (see VARARGIN)

% Choose default command line output for modemDPSK
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes modemDPSK wait for user response (see UIRESUME)
% uiwait(handles.okno);


% --- Outputs from this function are returned to the command line.
function varargout = modemDPSK_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function message_txt_Callback(hObject, eventdata, handles)
% hObject    handle to message_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of message_txt as text
%        str2double(get(hObject,'String')) returns contents of message_txt as a double


% --- Executes during object creation, after setting all properties.
function message_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to message_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function decodedMessage_txt_Callback(hObject, eventdata, handles)
% hObject    handle to decodedMessage_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of decodedMessage_txt as text
%        str2double(get(hObject,'String')) returns contents of decodedMessage_txt as a double


% --- Executes during object creation, after setting all properties.
function decodedMessage_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to decodedMessage_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in mod_demod_btn.
function mod_demod_btn_Callback(hObject, eventdata, handles)
% obs�uga przycisku moduluj/demoduluj
if isempty(get(handles.message_txt,'String'))
    errordlg('Nie wprowadzono wiadomosci (przykladowa poprawna forma to:10010101).');
    return 
end

bits = get(handles.message_txt,'String')-'0';%zamiana stringa na liczby

if (not(is_bits(bits))) %sprawdzam czy wprowadzono poprawnie bity
    errordlg('Wiadomosc nie sk�ada si� z bitow (przykladowa poprawna forma to:10010101).'); 
    return
end

number = get(handles.snr_txt,'String'); %pobieram zaznaczony snr
 
if not(isDigit(number))
        errordlg('SNR nie jest liczba calkowita.'); 
        return
end

    InterfaceObj=findobj(handles.okno,'Enable','on');
    set(InterfaceObj,'Enable','off');
    if get(handles.dpsk_radio,'Value') %sprawdzam czy radio button jest zaznaczony       
            wynik = plot_DPSK(bits,str2num(number));%rysuje odpowiednie wykresy i zwracam odzykane bity
            wynik = num2str(wynik);
            wynik(wynik==' ')=[];
            set(handles.decodedMessage_txt,'String',num2str(wynik));%aktualizuje edit'a o zdemodulowany ci�g bit�w
              
    end
     
    if get(handles.bpsk_radio,'Value')
          wynik =wykres_BPSK(bits,str2num(number));%rysuje odpowiednie wykresy i zwracam odzykane bity
          wynik = num2str(wynik);
          wynik(wynik==' ')=[];
          set(handles.decodedMessage_txt,'String',num2str(wynik));          
    end  
    
 set(InterfaceObj,'Enable','on');
 figure(handles.okno);

% --- Executes on button press in ber_btn.
function ber_btn_Callback(hObject, eventdata, handles)
% hObject    handle to ber_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
InterfaceObj=findobj(handles.okno,'Enable','on');
set(InterfaceObj,'Enable','off');
%pytanie czy wczytac ju� wczesniej obliczone dane
choice = questdlg('Wczytac obliczone dane? Symulacja od poczatku bedzie trwac bardzo dlugo.', ...
                            'Wczytanie danych','Tak','Nie','Nie');
    if get(handles.dpsk_radio,'Value')           
                switch choice
                    case 'Tak'
                        plot_DPSK_BPSK();
                    case 'Nie'
                        msgbox('Operacja w toku, prosze czekac...');
                        wynik = ber_DPSK(100000);              
                end         
    end
    
    if get(handles.bpsk_radio,'Value')
            switch choice
                    case 'Tak'
                        plot_DPSK_BPSK();
                    case 'Nie'
                        msgbox('Operacja w toku, prosze czekac...');
                        wynik = ber_BPSK(100000);    
              end
      end     
    
 set(InterfaceObj,'Enable','on');
 figure(handles.okno);

% --- Executes on selection change in snr_value.
function snr_value_Callback(hObject, eventdata, handles)
% hObject    handle to snr_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns snr_value contents as cell array
%        contents{get(hObject,'Value')} returns selected item from snr_value


% --- Executes during object creation, after setting all properties.
function snr_value_CreateFcn(hObject, eventdata, handles)
% hObject    handle to snr_value (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in losuj_btn.
function losuj_btn_Callback(hObject, eventdata, handles)
% hObject    handle to losuj_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    wynik = randBits(10);
    wynik = num2str(wynik);
    wynik(wynik==' ')=[];
    set(handles.message_txt,'String',num2str(wynik));


% --- Executes on button press in odzyskaj_btn.
function odzyskaj_btn_Callback(hObject, eventdata, handles)
% hObject    handle to odzyskaj_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    if get(handles.dpsk_radio,'Value')
        errordlg('W modulacji DPSK nie ma potrzeby odzyskiwac nosna');
       return 
    end
    if isempty(get(handles.message_txt,'String'))
       errordlg('Nie wprowadzono wiadomosci (przykladowa poprawna forma to:10010101).');
       return 
    end

    bits = get(handles.message_txt,'String')-'0';%zamiana stringa na liczby

    if (not(is_bits(bits))) %sprawdzam czy wprowadzono poprawnie bity
      errordlg('Wiadomosc nie sklada sie z bitow (przykladowa poprawna forma to:10010101).'); 
      return
    end
    
    if(length(bits)<3)
        errordlg('Wprowadz przynajmniej 3 bity.'); 
        return
    end

    number = get(handles.snr_txt,'String'); %pobieram zaznaczony snr 
    if isDigit(number)
        odzysknosnej(bits,str2num(number));
    else
        errordlg('SNR nie jest liczba calkowita.'); 
        return
    end
    figure(handles.okno);

function snr_txt_Callback(hObject, eventdata, handles)
% hObject    handle to snr_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of snr_txt as text
%        str2double(get(hObject,'String')) returns contents of snr_txt as a double


% --- Executes during object creation, after setting all properties.
function snr_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to snr_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

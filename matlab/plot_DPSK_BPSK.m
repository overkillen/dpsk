function [  ] = plot_DPSK_BPSK(  )
%PLOT_DPSK_BPSK Summary of this function goes here
%   Funkcja wczytuje obliczone wcze�niej wyniki symulacji BER dla BPSK i
%   DPSK
    f3 = figure(3);
    setPosition(f3,true);
    SNRdB = -4:13;
    EbN0dB=SNRdB(1):0.001:SNRdB(length(SNRdB));
    EbN0lin=10.^(EbN0dB/10);
    Pb_DPSK = 0.5*exp(-EbN0lin);    
    simulatedPB_DPSK = importdata('10x1000000DPSK.mat');      %1000000 bit�w razy 10
    Pb_BPSK = (1/2)*erfc(sqrt(EbN0lin));
    simulatedPB_BPSK  =  importdata('10x1000000BPSK.mat');    %1000000 bit�w razy 10
    semilogy(SNRdB,simulatedPB_DPSK,'x', ...
                   EbN0dB ,Pb_DPSK,...
                   SNRdB,simulatedPB_BPSK,'xr',...
                   EbN0dB ,Pb_BPSK,'b');
    title('Bit error rate DPSK i BPSK')
    xlabel('SNR [dB]');
    ylabel('BER')
    legend('Doswiadczalnie DPSK','Teoretycznie DPSK','Doswiadczalnie BPSK','Teoretycznie BPSK')
    
   
end


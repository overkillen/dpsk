function [out ] = randBits(bitsLength )
%Funkcja generuje bity
random_bits = randi([ 0 1],1,bitsLength);
out = random_bits;
end


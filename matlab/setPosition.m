function [ ] = setPosition( f,left )
%SETPOSITION Summary of this function goes here
%   Detailed explanation goes here
set(0,'Units','pixels') 
scnsize = get(0,'ScreenSize');
fig1 = f;
position = get(fig1,'Position');
outerpos = get(fig1,'OuterPosition');
borders = outerpos - position;
edge = -borders(1)/2;

    pos1 = [edge,...
        scnsize(4) * (1/4),...
        scnsize(3)/2 - edge,...
        scnsize(4)*3/4];

    pos2 = [scnsize(3)/2 + edge,...
        pos1(2),...
        pos1(3),...
        pos1(4)];
if left
    set(fig1,'OuterPosition',pos1) 
else
    set(fig1,'OuterPosition',pos2) 
end


end

